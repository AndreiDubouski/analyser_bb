#!/usr/bin/env python
""" This script use for analyse detection_val_log"""
from multiprocessing import Process, Manager
import mmap
import re
from random import uniform

import argparse
import sys
import json

IOU_THRESHOLD = 0.5
LITTLE_DELTA = 0.03
BIGGER_DELTA = 0.3


def parse(input_args):
    """ Function, which parse CLI
    example usage '-in ./data/detection_val_log.txt -out ./data/res.json'
    :param input_args:
    :return:
    """
    parser = argparse.ArgumentParser(
        description='this program, which find optimal threshold for recognized objects.',
        add_help=False)
    parser.add_argument('-in', '--input', type=str, help='input file path.\r\n')
    parser.add_argument('-out', '--output', type=str, help='output file path.\r\n')
    parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                        help='Show help message and exit.\r\n')

    try:
        return parser.parse_args(args=input_args)
    except argparse.ArgumentError:
        print('Use -h option for help on using.')


class TrueData:
    """ This is a class for true data. """

    def __init__(self, name, left, top, right, bottom):
        self.name = name
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom

    def __str__(self):
        return f'name:{self.name}'


class RecogniseData(TrueData):
    """ This is a class for recognised data. """
    def __init__(self, name, left, top, right, bottom, confidence):
        TrueData.__init__(self, name, left, top, right, bottom)
        self.confidence = confidence

    def __str__(self):
        return f'name:{self.name}, confidence:{self.confidence}'


class DataObject:
    """ This is a class used for learning and collecting data. """
    def __init__(self, name, threshold=0.0):
        self.name = name
        self.l_confidence = []
        self.l_iou = []
        self.l_present = []  # 1 if object present in scene else 0
        self.f1_score = 0
        self.threshold = threshold

    def __str__(self):
        return f'Name={self.name}, f1={self.f1_score}, threshold={self.threshold}'

    def __repr__(self):
        return f'Name={self.name}, f1={self.f1_score}, threshold={self.threshold}'


def get_area_overlap_from_bb(l_f, t_f, r_f, b_f, l_s, t_s, r_s, b_s):
    """ Function, which calculate overlap two BB
    # First - left,top,right,bottom
    # Second - left,top,right,bottom
    Object First
    :param l_f: left
    :param t_f: top
    :param r_f: right
    :param b_f: bottom
    Object Second
    :param l_s: left
    :param t_s: top
    :param r_s: right
    :param b_s: bottom
    :return:
    """
    x_b = min(r_f, r_s)
    x_a = max(l_f, l_s)
    y_b = min(t_f, t_s)
    y_a = max(b_f, b_s)
    inter_area = max(0, x_b - x_a) * max(0, y_b - y_a)
    return inter_area


def get_area_union_from_bb(l_f, t_f, r_f, b_f, l_s, t_s, r_s, b_s):
    """ Function, which calculate union two BB
    # First - left,top,right,bottom
    # Second - left,top,right,bottom
    Object First
    :param l_f: left
    :param t_f: top
    :param r_f: right
    :param b_f: bottom
    Object Second
    :param l_s: left
    :param t_s: top
    :param r_s: right
    :param b_s: bottom
    :return:
    """
    x_b = max(r_f, r_s)
    x_a = min(l_f, l_s)
    y_b = max(t_f, t_s)
    y_a = min(b_f, b_s)
    full_box_area = (y_b - y_a) * (x_b - x_a)
    rest = (max(l_f, l_s) - x_a) * (max(b_f, b_s) - y_a)
    union_area = full_box_area - rest * 2
    return union_area


def get_iou(overlap_area, union_area):
    """ Function, which calculate Intersection Over Union (IOU)
    :param overlap_area:
    :param union_area:
    :return:
    """
    return 0 if overlap_area == 0 else overlap_area / union_area


def get_precision(t_p, f_p):
    """ Function, which calculate the precision
    :param t_p: Float true_positive
    :param f_p: Float false_positive
    :return precision: Float
    """
    return t_p / (t_p + f_p) if t_p > 0 else 0


def get_recall(t_p, f_n):
    """ Function, which calculate the recall
    :param t_p: Float true_positive
    :param f_n: Float false_negative
    :return recall: Float
    """
    return t_p / (t_p + f_n) if t_p > 0 else 0


def get_f1_score(precision, recall):
    """ Function, which calculate the F1-measure
    :param precision:
    :param recall:
    :return f1_score:
    """
    return 0.0 if recall == 0 else 2 * precision * recall / (precision + recall)


def recalculate_f1(value, threshold):
    """ Function use for recalculate f1_score
    :param value: объект, который содержит предварительно обработанные данные
    :param threshold: float number between 0.0 and 1.0
    :return f1_score: calculate new f1_score
    """
    present = value.l_present
    confidence = value.l_confidence
    t_p, f_p, f_n = 0, 0, 0
    # for i in iter_range:
    for ind, item_present in enumerate(present):
        conf = confidence[ind]
        if item_present:
            if conf > threshold:
                t_p += 1
            else:
                f_n += 1
        elif conf > threshold:
            f_p += 1

    return get_f1_score(get_precision(t_p, f_p), get_recall(t_p, f_n))


def calculate_f1_for_preprocessed_data(collected_data):
    for value in collected_data.values():
        threshold = value.threshold
        present = value.l_present
        confidence = value.l_confidence
        length = len(present)
        iter_range = range(length)

        t_p = len([1 for i in iter_range if present[i] == 1 and confidence[i] > threshold])
        f_p = len([1 for i in iter_range if present[i] == 0 and confidence[i] > threshold])
        f_n = len([1 for i in iter_range if present[i] == 1 and confidence[i] <= threshold])
        value.f1_score = get_f1_score(get_precision(t_p, f_p), get_recall(t_p, f_n))


def get_true_objects_from_str(string):
    """ Function, which get dict from str
    :param string: for example 'Blouse,72,71,215,277'
    :return true_dict: dict
    """
    true_d = re.split(';|,', string)
    true_dict = dict()
    for index in range(len(true_d) // 5):
        name, l, t, r, b = true_d[index * 5: index * 5 + 5]
        # true_dict[n] = TrueData(n, int(l), int(t), int(r), int(b))
        true_dict[name] = TrueData(name, int(l), int(b), int(r), int(t))
    return true_dict


def get_recognize_objects_from_str(string):
    """ Function, which get dict from str
    :param string: for example 'Jacket,48,56,144,253,0.21688978374'
    :return true_dict: dict
    """
    rec_d = re.split(';|,', string)
    recognize_dict = dict()
    for i in range(len(rec_d) // 6):
        name, l, t, r, b, conf = rec_d[i * 6: i * 6 + 6]
        recognize_dict[name] = RecogniseData(name, int(l), int(b), int(r), int(t), float(conf))
    return recognize_dict


def calculate_iou(real, rec):
    overlap = get_area_overlap_from_bb(real.left, real.top, real.right,
                                       real.bottom, rec.left, rec.top, rec.right, rec.bottom)
    union = get_area_union_from_bb(real.left, real.top, real.right,
                                   real.bottom, rec.left, rec.top, rec.right, rec.bottom)
    iou = get_iou(overlap, union)
    return iou


def compare_true_recognised(preprocessed_data_dict, real, rec):
    # collect and preprocess data
    for k, value in rec.items():
        if k in preprocessed_data_dict:
            df_obj = preprocessed_data_dict[k]
        else:
            df_obj = DataObject(k)
            preprocessed_data_dict[k] = df_obj
        df_obj.l_confidence.append(rec[k].confidence) if k in rec else df_obj.l_confidence.append(0.0)

        if k in real:
            df_obj.l_iou.append(calculate_iou(real[k], value))
            df_obj.l_present.append(1)  # объект на сцене действительно присутствует
        else:
            df_obj.l_iou.append(0.0)
            df_obj.l_present.append(0)  # на сцене объект отсутствует

    keys = real.keys() - rec.keys()
    for k in keys:
        if k in preprocessed_data_dict:
            df_obj = preprocessed_data_dict[k]
        else:
            df_obj = DataObject(k)
            preprocessed_data_dict[k] = df_obj
        df_obj.l_confidence.append(0.0)
        df_obj.l_iou.append(0.0)
        df_obj.l_present.append(1)  # объект на сцене действительно присутствует


def parse_line(preprocessed_data_dict, line):
    true_str_data, recognise_str_data = line.split('--')

    true_d = get_true_objects_from_str(true_str_data)
    rec_d = get_recognize_objects_from_str(recognise_str_data)
    compare_true_recognised(preprocessed_data_dict, true_d, rec_d)  # fitness function


def mp_recalculate_f1(value, threshold):
    """ Function use for recalculate f1_score
    :param value: object, which consist of collected and preprocessed data about one class
    :param threshold: float number between 0.0 and 1.0
    :return f1_score: calculate new f1_score
    """
    present = value['l_present']
    confidence = value['l_confidence']
    # length = len(present)
    # iter_range = range(length)
    t_p, f_p, f_n = 0, 0, 0
    # for i in iter_range:
    for index, item in enumerate(present):
        conf = confidence[index]
        # pres = present[i]
        # if item == 1:
        if item:
            if conf > threshold:
                t_p += 1
            else:
                f_n += 1
        elif conf > threshold:
            f_p += 1

    return get_f1_score(get_precision(t_p, f_p), get_recall(t_p, f_n))


def mp_find_optimum(collected_data, way):
    best = dict()
    for _ in range(10):

        i_3, i_2, i_1, i_0 = 0.0, 0.0, 0.0, 0.0
        for _ in range(30):

            i_0 = uniform(0.0, 1.0) if i_0 < 0.0 or i_0 > 1.0 else way + uniform(0.0, 0.03)
            f_0 = mp_recalculate_f1(collected_data, i_0)

            i_1 = uniform(0.0, 1.0) if i_1 < 0.0 or i_1 > 1.0 else way - uniform(0.0, 0.03)
            f_1 = mp_recalculate_f1(collected_data, i_1)

            i_2 = uniform(0.0, 1.0) if i_2 < 0.0 or i_2 > 1.0 else way + uniform(0.0, 0.3)
            f_2 = mp_recalculate_f1(collected_data, i_2)

            i_3 = uniform(0.0, 1.0) if i_3 < 0.0 or i_3 > 1.0 else way - uniform(0.0, 0.3)
            f_3 = mp_recalculate_f1(collected_data, i_3)

            if f_0 >= max(f_1, f_2, f_3):
                if 1.0 > i_0 > 0.0:
                    way = i_0
            elif f_1 >= max(f_0, f_2, f_3):
                if 1.0 > i_1 > 0.0:
                    way = i_1
            elif f_2 >= max(f_0, f_1, f_3):
                if 1.0 > i_2 > 0.0:
                    way = i_2
            else:
                if 1.0 > i_3 > 0.0:
                    way = i_3
        best[way] = mp_recalculate_f1(collected_data, way)
    return max(best, key=lambda key: best[key])


def mp_analyse(collected_data, return_dict):
    """ Function search optimal threshold using bee-algorithm
    :param collected_data: dict with preprocessed data
    :return: threshold
    """
    best = dict()
    way = mp_find_optimum(collected_data, uniform(0.0, 1.0))
    best[way] = mp_recalculate_f1(collected_data, way)
    for _ in range(5):
        way = mp_find_optimum(collected_data, way)
        best[way] = mp_recalculate_f1(collected_data, way)

    result = max(best, key=lambda key: best[key])
    collected_data['threshold'] = result
    key = str(collected_data['name'])
    # return_dict[name] = key
    return_dict[key] = result


def line_count(filename):
    """ Function, which get number of lines
    :param filename: path to file
    :return :
    """
    with open(filename) as f:
        return sum(1 for _ in f)


def read_faster(filename):
    preprocessed_data_dict = {}
    with open(filename, 'rb+') as file:
        # memory-map the file, size 0 means whole file
        buf = mmap.mmap(file.fileno(), 0)
        for _ in range(line_count(filename)):
            parse_line(preprocessed_data_dict, buf.readline().decode())

        buf.close()  # close the map

    calculate_f1_for_preprocessed_data(preprocessed_data_dict)
    return preprocessed_data_dict


if __name__ == '__main__':

    args = sys.argv[1:]
    parsed = parse(args)
    data = read_faster(parsed.input)

    # next
    with Manager() as manager:
        L = manager.list()  # shared between processes.

        for v in data.values():
            L.append(v.__dict__)

        workers = []
        result = manager.dict()
        for item in L:
            p = Process(target=mp_analyse, args=(item, result))
            workers.append(p)
            p.start()

        for worker in workers:
            worker.join()

        with open(parsed.output, 'w') as file:
            json.dump(result.items(), file, indent=4)
